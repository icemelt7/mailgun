import React from 'react';

const ActionEmailPreview = ({values: {text1, text2, actionText}}) => {
  return <table className="body-wrap" style={{fontFamily: '"Helvetica Neue",Helvetica,Arial,sans-serif', boxSizing: 'border-box', fontSize: '14px', width: '100%', backgroundColor: '#f6f6f6', margin: 0}} bgcolor="#f6f6f6">
  <tbody><tr style={{fontFamily: '"Helvetica Neue",Helvetica,Arial,sans-serif', boxSizing: 'border-box', fontSize: '14px', margin: 0}}>
      <td style={{fontFamily: '"Helvetica Neue",Helvetica,Arial,sans-serif', boxSizing: 'border-box', fontSize: '14px', verticalAlign: 'top', margin: 0}} valign="top" />
      <td className="container" width={600} style={{fontFamily: '"Helvetica Neue",Helvetica,Arial,sans-serif', boxSizing: 'border-box', fontSize: '14px', verticalAlign: 'top', display: 'block !important', maxWidth: '600px !important', clear: 'both !important', margin: '0 auto'}} valign="top">
        <div className="content" style={{fontFamily: '"Helvetica Neue",Helvetica,Arial,sans-serif', boxSizing: 'border-box', fontSize: '14px', maxWidth: '600px', display: 'block', margin: '0 auto', padding: '20px'}}>
          <table className="main" width="100%" cellPadding={0} cellSpacing={0} itemProp="action" itemScope itemType="http://schema.org/ConfirmAction" style={{fontFamily: '"Helvetica Neue",Helvetica,Arial,sans-serif', boxSizing: 'border-box', fontSize: '14px', borderRadius: '3px', backgroundColor: '#fff', margin: 0, border: '1px solid #e9e9e9'}} bgcolor="#fff">
            <tbody><tr style={{fontFamily: '"Helvetica Neue",Helvetica,Arial,sans-serif', boxSizing: 'border-box', fontSize: '14px', margin: 0}}>
                <td className="content-wrap" style={{fontFamily: '"Helvetica Neue",Helvetica,Arial,sans-serif', boxSizing: 'border-box', fontSize: '14px', verticalAlign: 'top', margin: 0, padding: '20px'}} valign="top">
                  <meta itemProp="name" content="Confirm Email" style={{fontFamily: '"Helvetica Neue",Helvetica,Arial,sans-serif', boxSizing: 'border-box', fontSize: '14px', margin: 0}} />
                  <table width="100%" cellPadding={0} cellSpacing={0} style={{fontFamily: '"Helvetica Neue",Helvetica,Arial,sans-serif', boxSizing: 'border-box', fontSize: '14px', margin: 0}}>
                    <tbody><tr style={{fontFamily: '"Helvetica Neue",Helvetica,Arial,sans-serif', boxSizing: 'border-box', fontSize: '14px', margin: 0}}>
                        <td className="content-block" style={{fontFamily: '"Helvetica Neue",Helvetica,Arial,sans-serif', boxSizing: 'border-box', fontSize: '14px', verticalAlign: 'top', margin: 0, padding: '0 0 20px'}} valign="top">
                          {text1}
                        </td>
                      </tr>
                      <tr style={{fontFamily: '"Helvetica Neue",Helvetica,Arial,sans-serif', boxSizing: 'border-box', fontSize: '14px', margin: 0}}>
                        <td className="content-block" style={{fontFamily: '"Helvetica Neue",Helvetica,Arial,sans-serif', boxSizing: 'border-box', fontSize: '14px', verticalAlign: 'top', margin: 0, padding: '0 0 20px'}} valign="top">
                          {text2}
                        </td>
                      </tr>
                      <tr style={{fontFamily: '"Helvetica Neue",Helvetica,Arial,sans-serif', boxSizing: 'border-box', fontSize: '14px', margin: 0}}>
                        <td className="content-block" itemProp="handler" itemScope itemType="http://schema.org/HttpActionHandler" style={{fontFamily: '"Helvetica Neue",Helvetica,Arial,sans-serif', boxSizing: 'border-box', fontSize: '14px', verticalAlign: 'top', margin: 0, padding: '0 0 20px'}} valign="top">
                          <a href="{actionLink}" className="btn-primary" itemProp="url" style={{fontFamily: '"Helvetica Neue",Helvetica,Arial,sans-serif', boxSizing: 'border-box', fontSize: '14px', color: '#FFF', textDecoration: 'none', lineHeight: '2em', fontWeight: 'bold', textAlign: 'center', cursor: 'pointer', display: 'inline-block', borderRadius: '5px', textTransform: 'capitalize', backgroundColor: '#348eda', margin: 0, borderColor: '#348eda', borderStyle: 'solid', borderWidth: '10px 20px'}}>{actionText}</a>
                        </td>
                      </tr>
                      <tr style={{fontFamily: '"Helvetica Neue",Helvetica,Arial,sans-serif', boxSizing: 'border-box', fontSize: '14px', margin: 0}}>
                        <td className="content-block" style={{fontFamily: '"Helvetica Neue",Helvetica,Arial,sans-serif', boxSizing: 'border-box', fontSize: '14px', verticalAlign: 'top', margin: 0, padding: '0 0 20px'}} valign="top">
                          Thanks — Linkbook Team
                        </td>
                      </tr>
                    </tbody></table>
                </td>
              </tr>
            </tbody></table>
        </div>
      </td>
      <td style={{fontFamily: '"Helvetica Neue",Helvetica,Arial,sans-serif', boxSizing: 'border-box', fontSize: '14px', verticalAlign: 'top', margin: 0}} valign="top" />
    </tr>
  </tbody></table>
}

export default ActionEmailPreview;