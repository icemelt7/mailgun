import React from "react";

const Input = ({ label, small, name, values, onChange, multiLine }) => {
  return (
    <div>
      {label && <label className="block mt-4">{label}</label>}
      {small && (
        <small className="block mb-2 w-64 text-gray-600 text-xs">{small}</small>
      )}
      {multiLine ? (
        <textarea
          rows={5}
          onChange={onChange}
          value={values[name] ? values[name] : ""}
          name={name}
          className="block appearance-none w-64 bg-white border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded leading-tight focus:outline-none"
        />
      ) : (
        <input
          onChange={onChange}
          value={values[name] ? values[name] : ""}
          name={name}
          className="block appearance-none w-64 bg-white border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded leading-tight focus:outline-none"
        />
      )}
    </div>
  );
};

export default Input;
