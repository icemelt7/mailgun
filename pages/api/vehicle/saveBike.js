// https://www.automark.pk/wp-json/acf/v3/motorcycle

// var apiPromise = WPAPI.discover("https://www.automark.pk").then(function(site) {
//   return site.auth({
//     username: "icemelt7",
//     password: "jojo33"
//   });
// });
import got from 'got';
export default (req, res) => {
  const {
    query: { id, price }
  } = req;

  got
    .post(
      `https://www.automark.pk/wp-json/acf/v3/motorcycle/${id}`,
      {
        headers: {
          "Authorization": "Basic aWNlbWVsdDc6am9qbzMz"
        },
        body: {
          "fields": {
            "price": price
          }
        },
        json: true
      }
    )
    .then((r) => {
      console.log(r)
      res.end(JSON.stringify({ status: "success" }));
    })
    .catch(e => {
      res.end(JSON.stringify(e));
    });
};
// If default routes were detected, they are now available
//   site.motorcycles().then(function( bike ) {
//       console.log( bike );
//       res.end(JSON.stringify({ name: 'Nextjs' }))
//   }); // etc
//   res.end(JSON.stringify({ name: 'Nextjs' }))
// });
