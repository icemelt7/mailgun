import { useState } from "react";
import "../styles/index.css";
import Select from "../components/Select";
import Input from "../components/Input";
import ActionEmailPreview from "../components/ActionEmailPreview";

const useForm = () => {
  const [values, setValues] = useState({});

  const onChange = (e) => {
    setValues({...values, [e.target.name]: e.target.value});
  }

  return [values, onChange];
}
export default () => {
  const [listMode, setListMode] = useState("list");
  const [values, onChange] = useForm();
  return (
    <div className="xl:p-12 p-4 xl:m-12 shadow-2xl rounded bg-white">
      <h1 className="text-3xl text-red-700 font-bold text-center bg-gray-100 p-2 border-gray-300 border-b">
        Automark Emailer
      </h1>
      <form>
        <Select
          label="Email Type"
          onChange={() => {}}
          options={[
            {
              value: "newsletter",
              text: "Newsletter"
            },
            {
              value: "action-email",
              text: "Action Email"
            }
          ]}
        />

        <Select
          label="List or individual emails?"
          options={[
            {
              text: "List",
              value: "list"
            },
            {
              text: "Individual emails",
              value: "individual"
            }
          ]}
          onChange={value => setListMode(value)}
        />

        {listMode === "list" && (
          <Select
            onChange={() => {}}
            label="Mailing List"
            options={[
              {
                value: "newsletter",
                text: "Newsletter Subscribers"
              },
              {
                text: "Linkbook Users",
                value: "linkbook"
              }
            ]}
          />
        )}

        {listMode === "individual" && (
          <Input values={values} onChange={onChange}
          name="emailInput"
          label="Enter Email Address" small="Email address can be comma separated like this: bob@mail.com, mary@mail.com"/>
        )}

        <div className="border-green-500 border-b w-full p-1 my-4"></div>

        {/* -- Newsletter Start -- */}
        
        {/* --  Newsletter End  -- */}

        {/* Action Email */}
        <div className="xl:flex">
          <div className="xl:w-3/12">
          <h2 className="text-2xl">Action Email Creator</h2>

          <Input label="Email Title" onChange={onChange} values={values} name="emailTitle"/>
          <Input label="Text Block 1" onChange={onChange} values={values} name="text1" multiLine/>
          <Input label="Text Block 2" onChange={onChange} values={values} name="text2" multiLine/>
          <Input label="Action Link" onChange={onChange} values={values} name="actionLink"/>
          <Input label="Action Text" onChange={onChange} values={values} name="actionText"/>
          </div>
          <div className="xl:w-9/12 mt-8 xl:mt-0">
            <h2 className="text-2xl xl:mb-10">Email Preview</h2>
            <div>
              <ActionEmailPreview values={values}/>
            </div>
            <input 
              className="bg-blue-600 py-2 px-4 rounded text-white font-semibold mt-4
              hover:bg-blue-800 shadow-md hover:shadow-lg" type="submit" value="Send Email"/>
          </div>
        </div>
      </form>
    </div>
  );
};
