const mysql = require('serverless-mysql')
const SQL = require('sql-template-strings')
const fetch = require('isomorphic-unfetch');
var API_KEY = "key-347ce58d94c889ddd3b1633bf3d95403";
// process.env.API_KEY;
var DOMAIN = 'mg.automark.pk';
//var DOMAIN = "sandboxe688729b6b354341bbc40a27cbe20d0e.mailgun.org";
var mailgun = require('mailgun-js')({apiKey: API_KEY, domain: DOMAIN});

const devMode = false;
const mailFunction = async (data) => {
  return new Promise((resolve, reject) => {    
    mailgun.messages().send(data, (error, body) => {
      if (error){
        console.log(error);
        reject(JSON.stringify(error));
      }else{
        resolve(body);
      }
    })
  })
}
const db = mysql({
  config: {
    host: '77.104.133.159',
    database: 'automar7_wp',
    user: 'automar7_icemelt',
    password: 'Jojo.3344'
  }
})

const query = async query => {
  try {
    const results = await db.query(query)
    // console.log(results)
    await db.end()
    return results
  } catch (error) {
    console.log(error)
    return { error }
  }
}

module.exports = async (req, res) => {
  const answer = await query("SELECT * FROM `wp_posts` WHERE post_status='publish' ORDER BY `wp_posts`.`post_date` DESC LIMIT 0,1");
  const email_sent = parseInt(answer[0].email_sent, 10);
  if (email_sent === 1) {
    res.send({
      "message": "Email Already Sent"
    });
    return;
  };
  const postsRaw = await fetch('https://www.automark.pk/wp-json/wp/v2/posts/?per_page=1');
  const posts = await postsRaw.json();
  const post = posts[0];
  const heading = post.title.rendered;
  let html = `<article style="max-width: 500px;">
    <h1>${heading}</h1>
    <img width="500" style="display: block; margin: 0 auto;" src="${post.jetpack_featured_media_url}" />
    <p>
    ${post.excerpt.rendered}
    </p>
    <a href="${post.link}" style="color: #ffd6d6;
    text-decoration: none;
    background-color: #a90000;font-size: 12px;padding: 10px;border-radius: 12px;margin: 22px 20px;display: block;text-align: center;">Read The Full Article at Automark.pk</a>
    
  </article>`;
  
  const data = {
    from: 'Automark Newsletter <linkbook@mg.automark.pk>',
    to: 'subscribers@mg.automark.pk',
    subject: `${heading} - Update by Automark Magazine`,
    html,
    "o:tag" : [heading]
  };

  let emailResult = null;
  try {
    if (!devMode){
      emailResult = await mailFunction(data);
      const post_id = parseInt(answer[0].ID, 10);
      await query("UPDATE `wp_posts` SET `email_sent` = '1' WHERE `wp_posts`.`ID` = " + post_id);
    } else {
      console.log(JSON.stringify(data));
    }
    
  } catch (e) {
    emailResult = e;
  }

  if (devMode){
    res.send(html);
  } else {
    res.send({
      emailResult
    })
  }
}