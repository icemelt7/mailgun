var API_KEY = process.env.API_KEY;
// var DOMAIN = 'mg.automark.pk';
var DOMAIN = "sandboxe688729b6b354341bbc40a27cbe20d0e.mailgun.org";
var mailgun = require('mailgun-js')({apiKey: API_KEY, domain: DOMAIN});

const mailFunction = async (data) => {
  return new Promise((resolve, reject) => {    
    mailgun.messages().send(data, (error, body) => {
      if (error){
        reject(error);
      }else{
        resolve(body);
      }
    })
  })
}
module.exports = async (req, res) => {
  const method = req.method
  if (method !== "POST") {
    res.send("ONLY POST SUPPORTED");
    return;
  }

  
  let sendList = null;
  if (req.body.manualEmail){
    sendList = req.body.manualEmail;
  }
  const data = {
    from: 'Mustafa Hanif <linkbook@mg.automark.pk>',
    to: sendList.join(', '),
    subject: req.body.subject,
    text: req.body.text
  };

  let answer = null;
  try {
    answer = await mailFunction(data);
  } catch (e) {
    answer = e;
  }
  

  res.send(answer)
}